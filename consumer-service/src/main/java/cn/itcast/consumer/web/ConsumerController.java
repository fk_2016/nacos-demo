package cn.itcast.consumer.web;

import cn.itcast.consumer.config.RedisProperties;
import cn.itcast.consumer.feign.UserClient;
import cn.itcast.consumer.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author ITHEIMA
 */
@RestController
@RequestMapping("consumer")
public class ConsumerController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private RedisProperties redisProperties;
    @Autowired
    UserClient userClient;

//    @GetMapping("/rest/{id}")
//    public User consumerUserById(@PathVariable("id") String id){
////        String url = "http://localhost:8081/user/" + id;
//        String url = "http://user-service/user/" + id;
//        return restTemplate.getForObject(url, User.class);
//    }


    @GetMapping("/client/{id}")
    public User queryUserById(@PathVariable("id") String id){
        return userClient.queryUserById(id);
    }
    @GetMapping("/hello")
    public RedisProperties disUserById(){
        return redisProperties;
    }
}
