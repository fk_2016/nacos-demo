package cn.itcast.user.service;

import cn.itcast.user.entity.User;
import cn.itcast.user.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    @Value("${server.port}")
    private String port;

    public User queryById(String username) {

        User user = userMapper.findById(username);
        System.out.println("UserService...");

        user.setEmail("当前端口为==》"+port);
        return user;
    }
}
